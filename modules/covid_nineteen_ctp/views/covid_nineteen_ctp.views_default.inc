<?php

/**
 * @file
 * Provide default/example views for covid_nineteen_ctp module.
 */

/**
 * Implements hook_views_default_views().
 */
function covid_nineteen_ctp_views_default_views() {
  $view = new view();
  $view->name = 'covid_nineteen_ctp_example_views';
  $view->description = 'Example Views for COVID-19 Data from The COVID Tracking Project';
  $view->tag = 'default';
  $view->base_table = 'covid_nineteen_ctp';
  $view->human_name = 'COVID-19 Data Source: The COVID Tracking Project Example Views';
  $view->core = 7;
  $view->api_version = '3.0';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

  /* Display: Master */
  $handler = $view->new_display('default', 'Master', 'default');
  $handler->display->display_options['title'] = 'COVID-19 The COVID Tracking Project Example Views';
  $handler->display->display_options['use_more_always'] = FALSE;
  $handler->display->display_options['access']['type'] = 'none';
  $handler->display->display_options['cache']['type'] = 'none';
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['exposed_form']['type'] = 'basic';
  $handler->display->display_options['pager']['type'] = 'none';
  $handler->display->display_options['style_plugin'] = 'table';
  $handler->display->display_options['style_options']['columns'] = array(
    'counter' => 'counter',
    'state' => 'state',
    'positive' => 'positive',
    'totalTestResults' => 'totalTestResults',
    'positive_1' => 'positive_1',
    'totalTestResults_1' => 'totalTestResults_1',
    'expression' => 'expression',
    'death' => 'death',
    'death_1' => 'death_1',
    'expression_1' => 'expression_1',
    'grade' => 'grade',
    'dateModified' => 'dateModified',
    'hospitalizedCumulative' => 'hospitalizedCumulative',
    'hospitalizedCurrently' => 'hospitalizedCurrently',
  );
  $handler->display->display_options['style_options']['class'] = '';
  $handler->display->display_options['style_options']['default'] = 'positive';
  $handler->display->display_options['style_options']['info'] = array(
    'counter' => array(
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'state' => array(
      'sortable' => 1,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'positive' => array(
      'sortable' => 1,
      'default_sort_order' => 'desc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'totalTestResults' => array(
      'sortable' => 1,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'positive_1' => array(
      'sortable' => 1,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'totalTestResults_1' => array(
      'sortable' => 1,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'expression' => array(
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'death' => array(
      'sortable' => 1,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'death_1' => array(
      'sortable' => 1,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'expression_1' => array(
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'grade' => array(
      'sortable' => 1,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'dateModified' => array(
      'sortable' => 1,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'hospitalizedCumulative' => array(
      'sortable' => 1,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'hospitalizedCurrently' => array(
      'sortable' => 1,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
  );
  /* Field: Global: View result counter */
  $handler->display->display_options['fields']['counter']['id'] = 'counter';
  $handler->display->display_options['fields']['counter']['table'] = 'views';
  $handler->display->display_options['fields']['counter']['field'] = 'counter';
  $handler->display->display_options['fields']['counter']['label'] = '#';
  $handler->display->display_options['fields']['counter']['counter_start'] = '1';
  $handler->display->display_options['fields']['counter']['reverse'] = 0;
  /* Field: COVID-19 Data CTP: State */
  $handler->display->display_options['fields']['state']['id'] = 'state';
  $handler->display->display_options['fields']['state']['table'] = 'covid_nineteen_ctp';
  $handler->display->display_options['fields']['state']['field'] = 'state';
  /* Field: COVID-19 Data CTP: Positive */
  $handler->display->display_options['fields']['positive']['id'] = 'positive';
  $handler->display->display_options['fields']['positive']['table'] = 'covid_nineteen_ctp';
  $handler->display->display_options['fields']['positive']['field'] = 'positive';
  /* Field: COVID-19 Data CTP: Total Test Results */
  $handler->display->display_options['fields']['totalTestResults']['id'] = 'totalTestResults';
  $handler->display->display_options['fields']['totalTestResults']['table'] = 'covid_nineteen_ctp';
  $handler->display->display_options['fields']['totalTestResults']['field'] = 'totalTestResults';
  /* Field: COVID-19 Data CTP: Positive */
  $handler->display->display_options['fields']['positive_1']['id'] = 'positive_1';
  $handler->display->display_options['fields']['positive_1']['table'] = 'covid_nineteen_ctp';
  $handler->display->display_options['fields']['positive_1']['field'] = 'positive';
  $handler->display->display_options['fields']['positive_1']['label'] = 'Positive no comma';
  $handler->display->display_options['fields']['positive_1']['exclude'] = TRUE;
  $handler->display->display_options['fields']['positive_1']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['positive_1']['separator'] = '';
  /* Field: COVID-19 Data CTP: Total Test Results */
  $handler->display->display_options['fields']['totalTestResults_1']['id'] = 'totalTestResults_1';
  $handler->display->display_options['fields']['totalTestResults_1']['table'] = 'covid_nineteen_ctp';
  $handler->display->display_options['fields']['totalTestResults_1']['field'] = 'totalTestResults';
  $handler->display->display_options['fields']['totalTestResults_1']['label'] = 'Total Test Results no comma';
  $handler->display->display_options['fields']['totalTestResults_1']['exclude'] = TRUE;
  $handler->display->display_options['fields']['totalTestResults_1']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['totalTestResults_1']['separator'] = '';
  /* Field: Global: Math expression */
  $handler->display->display_options['fields']['expression']['id'] = 'expression';
  $handler->display->display_options['fields']['expression']['table'] = 'views';
  $handler->display->display_options['fields']['expression']['field'] = 'expression';
  $handler->display->display_options['fields']['expression']['label'] = 'Percent Positive';
  $handler->display->display_options['fields']['expression']['set_precision'] = TRUE;
  $handler->display->display_options['fields']['expression']['precision'] = '0';
  $handler->display->display_options['fields']['expression']['suffix'] = '%';
  $handler->display->display_options['fields']['expression']['expression'] = '([positive_1]/[totalTestResults_1])*100';
  /* Field: COVID-19 Data CTP: Death */
  $handler->display->display_options['fields']['death']['id'] = 'death';
  $handler->display->display_options['fields']['death']['table'] = 'covid_nineteen_ctp';
  $handler->display->display_options['fields']['death']['field'] = 'death';
  /* Field: COVID-19 Data CTP: Death */
  $handler->display->display_options['fields']['death_1']['id'] = 'death_1';
  $handler->display->display_options['fields']['death_1']['table'] = 'covid_nineteen_ctp';
  $handler->display->display_options['fields']['death_1']['field'] = 'death';
  $handler->display->display_options['fields']['death_1']['label'] = 'Death no comma';
  $handler->display->display_options['fields']['death_1']['exclude'] = TRUE;
  $handler->display->display_options['fields']['death_1']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['death_1']['hide_alter_empty'] = FALSE;
  $handler->display->display_options['fields']['death_1']['separator'] = '';
  /* Field: Global: Math expression */
  $handler->display->display_options['fields']['expression_1']['id'] = 'expression_1';
  $handler->display->display_options['fields']['expression_1']['table'] = 'views';
  $handler->display->display_options['fields']['expression_1']['field'] = 'expression';
  $handler->display->display_options['fields']['expression_1']['label'] = 'Fatality Rate';
  $handler->display->display_options['fields']['expression_1']['set_precision'] = TRUE;
  $handler->display->display_options['fields']['expression_1']['precision'] = '2';
  $handler->display->display_options['fields']['expression_1']['separator'] = '';
  $handler->display->display_options['fields']['expression_1']['suffix'] = '%';
  $handler->display->display_options['fields']['expression_1']['expression'] = '([death_1]/([positive_1]+.00000001))*100';
  /* Field: COVID-19 Data CTP: Hospitalized Currently */
  $handler->display->display_options['fields']['hospitalizedCurrently']['id'] = 'hospitalizedCurrently';
  $handler->display->display_options['fields']['hospitalizedCurrently']['table'] = 'covid_nineteen_ctp';
  $handler->display->display_options['fields']['hospitalizedCurrently']['field'] = 'hospitalizedCurrently';
  $handler->display->display_options['fields']['hospitalizedCurrently']['empty'] = '-';
  $handler->display->display_options['fields']['hospitalizedCurrently']['empty_zero'] = TRUE;
  /* Field: COVID-19 Data CTP: Hospitalized Cumulative */
  $handler->display->display_options['fields']['hospitalizedCumulative']['id'] = 'hospitalizedCumulative';
  $handler->display->display_options['fields']['hospitalizedCumulative']['table'] = 'covid_nineteen_ctp';
  $handler->display->display_options['fields']['hospitalizedCumulative']['field'] = 'hospitalizedCumulative';
  $handler->display->display_options['fields']['hospitalizedCumulative']['empty'] = '-';
  $handler->display->display_options['fields']['hospitalizedCumulative']['empty_zero'] = TRUE;
  $handler->display->display_options['fields']['hospitalizedCumulative']['hide_alter_empty'] = FALSE;
  /* Field: COVID-19 Data CTP: Grade */
  $handler->display->display_options['fields']['grade']['id'] = 'grade';
  $handler->display->display_options['fields']['grade']['table'] = 'covid_nineteen_ctp';
  $handler->display->display_options['fields']['grade']['field'] = 'grade';
  /* Field: COVID-19 Data CTP: Date Modified */
  $handler->display->display_options['fields']['dateModified']['id'] = 'dateModified';
  $handler->display->display_options['fields']['dateModified']['table'] = 'covid_nineteen_ctp';
  $handler->display->display_options['fields']['dateModified']['field'] = 'dateModified';
  $handler->display->display_options['fields']['dateModified']['date_format'] = 'uc_store';
  $handler->display->display_options['fields']['dateModified']['second_date_format'] = 'long';
  /* Sort criterion: COVID-19 Data CTP: Positive */
  $handler->display->display_options['sorts']['positive']['id'] = 'positive';
  $handler->display->display_options['sorts']['positive']['table'] = 'covid_nineteen_ctp';
  $handler->display->display_options['sorts']['positive']['field'] = 'positive';
  $handler->display->display_options['sorts']['positive']['order'] = 'DESC';

  /* Display: States */
  $handler = $view->new_display('block', 'States', 'block');
  $handler->display->display_options['defaults']['title'] = FALSE;
  $handler->display->display_options['title'] = 'COVID-19 COVID Tracking Project Data by State';
  $handler->display->display_options['display_description'] = 'States sorted by total number of confirmed cases';

  /* Display: US */
  $handler = $view->new_display('block', 'US summary', 'block_1');
  $handler->display->display_options['defaults']['title'] = FALSE;
  $handler->display->display_options['title'] = 'COVID-19 The COVID Tracking Project - US summary ';
  $handler->display->display_options['defaults']['group_by'] = FALSE;
  $handler->display->display_options['group_by'] = TRUE;
  $handler->display->display_options['defaults']['style_plugin'] = FALSE;
  $handler->display->display_options['style_plugin'] = 'table';
  $handler->display->display_options['style_options']['columns'] = array(
    'positive' => 'positive',
    'totalTestResults' => 'totalTestResults',
    'positive_1' => 'positive_1',
    'totalTestResults_1' => 'totalTestResults_1',
    'expression' => 'expression',
    'death' => 'death',
    'death_1' => 'death_1',
    'expression_1' => 'expression_1',
  );
  $handler->display->display_options['style_options']['class'] = '';
  $handler->display->display_options['style_options']['default'] = 'positive';
  $handler->display->display_options['style_options']['info'] = array(
    'positive' => array(
      'sortable' => 1,
      'default_sort_order' => 'desc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'totalTestResults' => array(
      'sortable' => 1,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'positive_1' => array(
      'sortable' => 1,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'totalTestResults_1' => array(
      'sortable' => 1,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'expression' => array(
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'death' => array(
      'sortable' => 1,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'death_1' => array(
      'sortable' => 1,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'expression_1' => array(
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
  );
  $handler->display->display_options['defaults']['style_options'] = FALSE;
  $handler->display->display_options['defaults']['row_plugin'] = FALSE;
  $handler->display->display_options['defaults']['row_options'] = FALSE;
  $handler->display->display_options['defaults']['fields'] = FALSE;
  /* Field: COVID-19 Data CTP: Positive */
  $handler->display->display_options['fields']['positive']['id'] = 'positive';
  $handler->display->display_options['fields']['positive']['table'] = 'covid_nineteen_ctp';
  $handler->display->display_options['fields']['positive']['field'] = 'positive';
  $handler->display->display_options['fields']['positive']['group_type'] = 'sum';
  /* Field: COVID-19 Data CTP: Total Test Results */
  $handler->display->display_options['fields']['totalTestResults']['id'] = 'totalTestResults';
  $handler->display->display_options['fields']['totalTestResults']['table'] = 'covid_nineteen_ctp';
  $handler->display->display_options['fields']['totalTestResults']['field'] = 'totalTestResults';
  $handler->display->display_options['fields']['totalTestResults']['group_type'] = 'sum';
  /* Field: COVID-19 Data CTP: Positive */
  $handler->display->display_options['fields']['positive_1']['id'] = 'positive_1';
  $handler->display->display_options['fields']['positive_1']['table'] = 'covid_nineteen_ctp';
  $handler->display->display_options['fields']['positive_1']['field'] = 'positive';
  $handler->display->display_options['fields']['positive_1']['group_type'] = 'sum';
  $handler->display->display_options['fields']['positive_1']['label'] = 'Positive no comma';
  $handler->display->display_options['fields']['positive_1']['exclude'] = TRUE;
  $handler->display->display_options['fields']['positive_1']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['positive_1']['separator'] = '';
  /* Field: COVID-19 Data CTP: Total Test Results */
  $handler->display->display_options['fields']['totalTestResults_1']['id'] = 'totalTestResults_1';
  $handler->display->display_options['fields']['totalTestResults_1']['table'] = 'covid_nineteen_ctp';
  $handler->display->display_options['fields']['totalTestResults_1']['field'] = 'totalTestResults';
  $handler->display->display_options['fields']['totalTestResults_1']['group_type'] = 'sum';
  $handler->display->display_options['fields']['totalTestResults_1']['label'] = 'Total Test Results no comma';
  $handler->display->display_options['fields']['totalTestResults_1']['exclude'] = TRUE;
  $handler->display->display_options['fields']['totalTestResults_1']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['totalTestResults_1']['separator'] = '';
  /* Field: Global: Math expression */
  $handler->display->display_options['fields']['expression']['id'] = 'expression';
  $handler->display->display_options['fields']['expression']['table'] = 'views';
  $handler->display->display_options['fields']['expression']['field'] = 'expression';
  $handler->display->display_options['fields']['expression']['group_type'] = 'sum';
  $handler->display->display_options['fields']['expression']['label'] = 'Percent Positive';
  $handler->display->display_options['fields']['expression']['set_precision'] = TRUE;
  $handler->display->display_options['fields']['expression']['precision'] = '0';
  $handler->display->display_options['fields']['expression']['suffix'] = '%';
  $handler->display->display_options['fields']['expression']['expression'] = '([positive_1]/[totalTestResults_1])*100';
  /* Field: COVID-19 Data CTP: Death */
  $handler->display->display_options['fields']['death']['id'] = 'death';
  $handler->display->display_options['fields']['death']['table'] = 'covid_nineteen_ctp';
  $handler->display->display_options['fields']['death']['field'] = 'death';
  $handler->display->display_options['fields']['death']['group_type'] = 'sum';
  /* Field: COVID-19 Data CTP: Death */
  $handler->display->display_options['fields']['death_1']['id'] = 'death_1';
  $handler->display->display_options['fields']['death_1']['table'] = 'covid_nineteen_ctp';
  $handler->display->display_options['fields']['death_1']['field'] = 'death';
  $handler->display->display_options['fields']['death_1']['group_type'] = 'sum';
  $handler->display->display_options['fields']['death_1']['label'] = 'Death no comma';
  $handler->display->display_options['fields']['death_1']['exclude'] = TRUE;
  $handler->display->display_options['fields']['death_1']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['death_1']['hide_alter_empty'] = FALSE;
  $handler->display->display_options['fields']['death_1']['separator'] = '';
  /* Field: Global: Math expression */
  $handler->display->display_options['fields']['expression_1']['id'] = 'expression_1';
  $handler->display->display_options['fields']['expression_1']['table'] = 'views';
  $handler->display->display_options['fields']['expression_1']['field'] = 'expression';
  $handler->display->display_options['fields']['expression_1']['group_type'] = 'sum';
  $handler->display->display_options['fields']['expression_1']['label'] = 'Fatality Rate';
  $handler->display->display_options['fields']['expression_1']['set_precision'] = TRUE;
  $handler->display->display_options['fields']['expression_1']['precision'] = '2';
  $handler->display->display_options['fields']['expression_1']['separator'] = '';
  $handler->display->display_options['fields']['expression_1']['suffix'] = '%';
  $handler->display->display_options['fields']['expression_1']['expression'] = '([death_1]/([positive_1]+.00000001))*100';
  /* Field: COVID-19 Data CTP: Date Modified */
  $handler->display->display_options['fields']['dateModified']['id'] = 'dateModified';
  $handler->display->display_options['fields']['dateModified']['table'] = 'covid_nineteen_ctp';
  $handler->display->display_options['fields']['dateModified']['field'] = 'dateModified';
  $handler->display->display_options['fields']['dateModified']['group_type'] = 'max';
  $handler->display->display_options['fields']['dateModified']['date_format'] = 'uc_store';
  $handler->display->display_options['fields']['dateModified']['second_date_format'] = 'long';

  $views = array();
  $views[$view->name] = $view;
  return $views;
}
