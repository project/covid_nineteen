<?php

/**
 * @file
 * Provide views data for covid_nineteen_ctp.module.
 */

/**
 * Implements hook_views_data().
 */
function covid_nineteen_ctp_views_data() {
  $data['covid_nineteen_ctp']['table']['group'] = t('COVID-19 Data CTP');
  $data['covid_nineteen_ctp']['table']['base'] = array(
    'field' => 'fips',
    'title' => t('COVID-19 Data Source: The COVID Tracking Project'),
    'help' => t('Contains data about COVID-19 infections compiled by the COVID Tracking Project.'),
    'weight' => -10,
  );

  $fields = covid_nineteen_ctp_get_fields();
  foreach ($fields as $field_id => $field) {
    $data_element = &$data['covid_nineteen_ctp'][$field_id];
    $data_element['title'] = $field_id;
    if (!empty($field['covid_nineteen_label'])) {
      $data_element['title'] = $field['covid_nineteen_label'];
    }
    $data_element['help'] = $field['description'];
    $field_type = $field['type'];
    if (!empty($field['covid_nineteen_views_data_type'])) {
      $field_type = $field['covid_nineteen_views_data_type'];
    }
    covid_nineteen_add_views_data_handlers($data_element, $field_type);
  }
  return $data;
}
