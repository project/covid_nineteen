COVID-19 Data Source: The COVID Tracking Project
==============

CONTENTS OF THIS FILE
---------------------

* Introduction
* Data License
* Requirements
* Installation
* Configuration
* Maintainers

INTRODUCTION
------------

The COVID-19 COVID Tracking Project Data Source module allows importing and
displaying of data compiled by The COVID Tracking Project (CTP) about the 2019
Novel Coronavirus COVID-19 (2019-nCoV).

The module includes a View with examples of how to display the data, and uses
Drupal Cron to schedule automatic updates of the data.

Neither this module or its maintainers are affiliated in any way with CTP, or
the sourcing of the data. This module simply provides a mechanism for
downloading and displaying data which has been made available to the public by
CTP. It does not make claims to the accuracy of the data being downloaded, and
does not guarantee the availability of the data.

This is a sub-module for the COVID-19 Data Sources module, and does not have a
project page of its own.

For a full description of the COVID-19 Data Sources module project visit the
project page:
<https://www.drupal.org/project/covid_nineteen>

To submit bug reports and feature suggestions, or to track changes:
<https://www.drupal.org/project/issues/covid_nineteen>

Visit the website for The Covid Tracking Project :
<https://covidtracking.com>.

DATA LICENSE
------------

The Covid Tracking Project releases its data under the Apache 2 License, Version
2: <https://github.com/COVID19Tracking/covid-tracking-data/blob/master/LICENSE>

REQUIREMENTS
------------

This module requires the following modules:

* [COVID-19 Data Sources](https://www.drupal.org/project/covid_nineteen)
* [Views](https://www.drupal.org/project/views)
* [Block](https://www.drupal.org/project/block)

INSTALLATION
------------

Ensure that all required modules are installed and enabled, then install as you
would normally install a contributed Drupal module:

* Also See <https://drupal.org/documentation/install/modules-themes/modules-7>
for further information.

CONFIGURATION
-------------

* Administration >> Configuration >> Web services >> COVID-19 Data Sources >>
COVID Tracking Project. You can enable/disable automatically updating the data,
how often it is updated, and can trigger an update by saving the form.
* Go to Administration >> Structure >> Views >> COVID-19 Data Source: The COVID
Tracking Project Example Views for examples of how to display the data.

MAINTAINERS
-----------

Current maintainers:

* Ben Greenberg (runswithscissors) - <http://drupal.org/user/3188825>
