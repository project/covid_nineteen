<?php

/**
 * @file
 * Administrative functions for the COVID Tracking Project data source.
 */

/**
 * Form constructor for the COVID-19 configuration form.
 *
 * @see covid_nineteen_ctp_menu()
 */
function covid_nineteen_ctp_admin_configure($form, &$form_state) {
  $form['intro'] = array(
    '#markup' => t('<h2>COVID Tracking Project</h2><p>Visit <a href="https://covidtracking.com/">https://covidtracking.com/</a> for information about the COVID Tracking Project.</p>'),
  );
  $form['covid_nineteen_ctp'] = array(
    '#tree' => TRUE,
    '#type' => 'fieldset',
  );
  drupal_load('module', 'covid_nineteen_ctp');
  $covid_nineteen_ctp = covid_nineteen_ctp_variable_get();
  $form['covid_nineteen_ctp']['last_us_data_hash'] = array(
    '#type' => 'value',
    '#default_value' => $covid_nineteen_ctp['last_us_data_hash'],
  );
  $form['covid_nineteen_ctp']['last_update_datetime'] = array(
    '#type' => 'value',
    '#default_value' => $covid_nineteen_ctp['last_update_datetime'],
  );
  $form['covid_nineteen_ctp']['auto_update'] = array(
    '#type' => 'checkbox',
    '#title' => t('Auto update'),
    '#description' => t('Automatically update the data using Drupal cron.'),
    '#default_value' => $covid_nineteen_ctp['auto_update'],
  );
  $last_update_check_time = $covid_nineteen_ctp['last_update_check_time'];
  $last_checked_string = t('never');
  if ($last_update_check_time) {
    $last_checked_string = format_date($last_update_check_time, 'short');
  }
  $form['covid_nineteen_ctp']['minutes_between_updates'] = array(
    '#type' => 'textfield',
    '#title' => t('Minutes between updates'),
    '#description' => t('How often to check if new data is available to be downloaded (last checked: @last-checked-string).', array('@last-checked-string' => $last_checked_string)),
    '#default_value' => $covid_nineteen_ctp['minutes_between_updates'],
    '#required' => TRUE,
  );
  $form['covid_nineteen_ctp']['last_update_check_time'] = array(
    '#type' => 'value',
    '#value' => $last_update_check_time,
  );
  $form['covid_nineteen_ctp']['api_us_data_url'] = array(
    '#type' => 'value',
    '#value' => $covid_nineteen_ctp['api_us_data_url'],
  );
  $form['covid_nineteen_ctp']['api_states_data_url'] = array(
    '#type' => 'value',
    '#value' => $covid_nineteen_ctp['api_states_data_url'],
  );
  $form['covid_nineteen_ctp']['update_data_now'] = array(
    '#type' => 'checkbox',
    '#title' => t('Update on submit'),
    '#default_value' => TRUE,
    '#description' => t('Update the data after submitting the form.'),
    '#required' => FALSE,
  );
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Submit'),
  );
  return $form;
}

/**
 * Validation handler for covid_nineteen_ctp_admin_configure().
 */
function covid_nineteen_ctp_admin_configure_validate($form, &$form_state) {
  $minutes_between_updates = &$form_state['values']['covid_nineteen_ctp']['minutes_between_updates'];
  if (!is_numeric($minutes_between_updates) || intval($minutes_between_updates) < 1) {
    form_set_error('minutes_between_updates', t('"Minutes between updates" must be a whole number greater than zero.'));
  }
  else {
    $minutes_between_updates = intval($minutes_between_updates);
  }
}

/**
 * Submit handler for covid_nineteen_ctp_admin_configure().
 */
function covid_nineteen_ctp_admin_configure_submit($form, &$form_state) {
  $covid_nineteen_ctp = $form_state['values']['covid_nineteen_ctp'];
  covid_nineteen_ctp_variable_set($covid_nineteen_ctp);
  if ($covid_nineteen_ctp['update_data_now']) {
    drupal_load('module', 'covid_nineteen_ctp');
    $user_initiated_update = &drupal_static('covid_nineteen_user_initiated_update', TRUE);
    $user_initiated_update = TRUE;
    covid_nineteen_ctp_update_data(TRUE);
  }
}
