<?php

/**
 * @file
 * Hooks provided by the OVID-19 Johns Hopkins University CSSE Data module.
 */

/**
 * @addtogroup hooks
 * @{
 */

/**
 * Allow modules to change the field names or their order within the data file.
 *
 * This is provided in case the order of the fields in the data file are
 * changed, and an update to this module is not yet available.
 *
 * The default field names and their order are:
 * $field_names = array(
 *   'fips',
 *   'admin2',
 *   'province_state',
 *   'country_region',
 *   'last_update',
 *   'lat',
 *   'long_',
 *   'confirmed',
 *   'deaths',
 *   'recovered',
 *   'active',
 *   'combined_key',
 * );
 *
 * @param &$field_names
 *   Array of strings.
 */
function hook_covid_nineteen_jhu_field_names_alter(&$field_names) {
  // Move the "last_update" field to the end.
  $field_names = array(
    'fips',
    'admin2',
    'province_state',
    'country_region',
    'lat',
    'long_',
    'confirmed',
    'deaths',
    'recovered',
    'active',
    'combined_key',
    'last_update',
  );
}

/**
 * @} End of "addtogroup hooks".
 */
