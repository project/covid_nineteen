<?php

/**
 * @file
 * Provide views data for covid_nineteen_jhu.module.
 */

/**
 * Implements hook_views_data().
 */
function covid_nineteen_jhu_views_data() {
  $data['covid_nineteen_jhu']['table']['group'] = t('COVID-19 Data JHU');
  $data['covid_nineteen_jhu']['table']['base'] = array(
    'field' => 'id',
    'title' => t('COVID-19 data from Johns Hopkins University'),
    'help' => t('Contains data about COVID-19 infections compiled by Johns Hopkins University.'),
    'weight' => -10,
  );

  $fields = covid_nineteen_jhu_get_fields();
  foreach ($fields as $field_id => $field) {
    $data_element = &$data['covid_nineteen_jhu'][$field_id];
    $data_element['title'] = $field_id;
    if (!empty($field['covid_nineteen_label'])) {
      $data_element['title'] = $field['covid_nineteen_label'];
    }
    $data_element['help'] = $field['description'];
    $field_type = $field['type'];
    if (!empty($field['covid_nineteen_views_data_type'])) {
      $field_type = $field['covid_nineteen_views_data_type'];
    }
    covid_nineteen_add_views_data_handlers($data_element, $field_type);
  }
  return $data;
}
