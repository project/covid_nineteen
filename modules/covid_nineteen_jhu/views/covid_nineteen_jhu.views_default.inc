<?php

/**
 * @file
 * Provide default/example views for covid_nineteen_jhu.module.
 */

/**
 * Implements hook_views_default_views().
 */
function covid_nineteen_jhu_views_default_views() {
  $view = new view();
  $view->name = 'covid_nineteen_jhu_example_views';
  $view->description = 'Example Views for COVID-19 Data from John Hopkins University';
  $view->tag = 'default';
  $view->base_table = 'covid_nineteen_jhu';
  $view->human_name = 'COVID-19 John Hopkins University Example Views';
  $view->core = 7;
  $view->api_version = '3.0';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

  /* Display: Master */
  $handler = $view->new_display('default', 'Master', 'default');
  $handler->display->display_options['title'] = 'COVID-19 John Hopkins University Example Views';
  $handler->display->display_options['use_more_always'] = FALSE;
  $handler->display->display_options['group_by'] = TRUE;
  $handler->display->display_options['access']['type'] = 'none';
  $handler->display->display_options['cache']['type'] = 'none';
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['exposed_form']['type'] = 'basic';
  $handler->display->display_options['pager']['type'] = 'none';
  $handler->display->display_options['style_plugin'] = 'table';
  /* Field: Global: View result counter */
  $handler->display->display_options['fields']['counter']['id'] = 'counter';
  $handler->display->display_options['fields']['counter']['table'] = 'views';
  $handler->display->display_options['fields']['counter']['field'] = 'counter';
  $handler->display->display_options['fields']['counter']['label'] = '#';
  $handler->display->display_options['fields']['counter']['counter_start'] = '1';
  $handler->display->display_options['fields']['counter']['reverse'] = 0;
  /* Field: COVID-19 Data JHU: Country */
  $handler->display->display_options['fields']['Country_Region']['id'] = 'Country_Region';
  $handler->display->display_options['fields']['Country_Region']['table'] = 'covid_nineteen_jhu';
  $handler->display->display_options['fields']['Country_Region']['field'] = 'Country_Region';
  /* Field: SUM(COVID-19 Data JHU: Confirmed cases) */
  $handler->display->display_options['fields']['Confirmed']['id'] = 'Confirmed';
  $handler->display->display_options['fields']['Confirmed']['table'] = 'covid_nineteen_jhu';
  $handler->display->display_options['fields']['Confirmed']['field'] = 'Confirmed';
  $handler->display->display_options['fields']['Confirmed']['group_type'] = 'sum';
  /* Field: SUM(COVID-19 Data JHU: Deaths) */
  $handler->display->display_options['fields']['Deaths']['id'] = 'Deaths';
  $handler->display->display_options['fields']['Deaths']['table'] = 'covid_nineteen_jhu';
  $handler->display->display_options['fields']['Deaths']['field'] = 'Deaths';
  $handler->display->display_options['fields']['Deaths']['group_type'] = 'sum';
  /* Field: SUM(COVID-19 Data JHU: Recovered) */
  $handler->display->display_options['fields']['Recovered']['id'] = 'Recovered';
  $handler->display->display_options['fields']['Recovered']['table'] = 'covid_nineteen_jhu';
  $handler->display->display_options['fields']['Recovered']['field'] = 'Recovered';
  $handler->display->display_options['fields']['Recovered']['group_type'] = 'sum';
  /* Field: SUM(COVID-19 Data JHU: Active cases) */
  $handler->display->display_options['fields']['Active']['id'] = 'Active';
  $handler->display->display_options['fields']['Active']['table'] = 'covid_nineteen_jhu';
  $handler->display->display_options['fields']['Active']['field'] = 'Active';
  $handler->display->display_options['fields']['Active']['group_type'] = 'sum';
  /* Sort criterion: SUM(COVID-19 Data JHU: Confirmed cases) */
  $handler->display->display_options['sorts']['Confirmed']['id'] = 'Confirmed';
  $handler->display->display_options['sorts']['Confirmed']['table'] = 'covid_nineteen_jhu';
  $handler->display->display_options['sorts']['Confirmed']['field'] = 'Confirmed';
  $handler->display->display_options['sorts']['Confirmed']['group_type'] = 'sum';
  $handler->display->display_options['sorts']['Confirmed']['order'] = 'DESC';

  /* Display: Countries */
  $handler = $view->new_display('block', 'Countries', 'covid_nineteen_jhu_example_block_countries');
  $handler->display->display_options['defaults']['title'] = FALSE;
  $handler->display->display_options['title'] = 'COVID-19 John Hopkins University Data by Country';
  $handler->display->display_options['display_description'] = 'Countries sorted by total number of Confirmed cases';
  $handler->display->display_options['defaults']['pager'] = FALSE;
  $handler->display->display_options['pager']['type'] = 'full';
  $handler->display->display_options['pager']['options']['items_per_page'] = '10';
  $handler->display->display_options['pager']['options']['offset'] = '0';
  $handler->display->display_options['pager']['options']['id'] = '0';
  $handler->display->display_options['pager']['options']['quantity'] = '9';

  /* Display: US States */
  $handler = $view->new_display('block', 'US States', 'covid_nineteen_jhu_example_block_us_states');
  $handler->display->display_options['defaults']['title'] = FALSE;
  $handler->display->display_options['title'] = 'COVID-19 John Hopkins University Data by US State';
  $handler->display->display_options['display_description'] = 'Note: JHU is no longer reporting Active or Recovered data at the state or county level';
  $handler->display->display_options['defaults']['pager'] = FALSE;
  $handler->display->display_options['pager']['type'] = 'none';
  $handler->display->display_options['pager']['options']['offset'] = '0';
  $handler->display->display_options['defaults']['fields'] = FALSE;
  /* Field: Global: View result counter */
  $handler->display->display_options['fields']['counter']['id'] = 'counter';
  $handler->display->display_options['fields']['counter']['table'] = 'views';
  $handler->display->display_options['fields']['counter']['field'] = 'counter';
  $handler->display->display_options['fields']['counter']['label'] = '#';
  $handler->display->display_options['fields']['counter']['counter_start'] = '1';
  $handler->display->display_options['fields']['counter']['reverse'] = 0;
  /* Field: COVID-19 Data JHU: State/Province */
  $handler->display->display_options['fields']['Province_State']['id'] = 'Province_State';
  $handler->display->display_options['fields']['Province_State']['table'] = 'covid_nineteen_jhu';
  $handler->display->display_options['fields']['Province_State']['field'] = 'Province_State';
  $handler->display->display_options['fields']['Province_State']['label'] = 'State';
  /* Field: SUM(COVID-19 Data JHU: Confirmed cases) */
  $handler->display->display_options['fields']['Confirmed']['id'] = 'Confirmed';
  $handler->display->display_options['fields']['Confirmed']['table'] = 'covid_nineteen_jhu';
  $handler->display->display_options['fields']['Confirmed']['field'] = 'Confirmed';
  $handler->display->display_options['fields']['Confirmed']['group_type'] = 'sum';
  /* Field: SUM(COVID-19 Data JHU: Deaths) */
  $handler->display->display_options['fields']['Deaths']['id'] = 'Deaths';
  $handler->display->display_options['fields']['Deaths']['table'] = 'covid_nineteen_jhu';
  $handler->display->display_options['fields']['Deaths']['field'] = 'Deaths';
  $handler->display->display_options['fields']['Deaths']['group_type'] = 'sum';
  $handler->display->display_options['defaults']['filter_groups'] = FALSE;
  $handler->display->display_options['defaults']['filters'] = FALSE;
  /* Filter criterion: COVID-19 Data JHU: Country */
  $handler->display->display_options['filters']['Country_Region']['id'] = 'Country_Region';
  $handler->display->display_options['filters']['Country_Region']['table'] = 'covid_nineteen_jhu';
  $handler->display->display_options['filters']['Country_Region']['field'] = 'Country_Region';
  $handler->display->display_options['filters']['Country_Region']['value'] = 'US';

  /* Display: Counties: New York */
  $handler = $view->new_display('block', 'Counties: New York', 'covid_nineteen_jhu_example_block_new_york_counties');
  $handler->display->display_options['defaults']['title'] = FALSE;
  $handler->display->display_options['title'] = 'COVID-19 John Hopkins University data for counties in the state of New York';
  $handler->display->display_options['display_description'] = 'Counties within the state of New York, US';
  $handler->display->display_options['defaults']['pager'] = FALSE;
  $handler->display->display_options['pager']['type'] = 'none';
  $handler->display->display_options['pager']['options']['offset'] = '0';
  $handler->display->display_options['defaults']['fields'] = FALSE;
  /* Field: Global: View result counter */
  $handler->display->display_options['fields']['counter']['id'] = 'counter';
  $handler->display->display_options['fields']['counter']['table'] = 'views';
  $handler->display->display_options['fields']['counter']['field'] = 'counter';
  $handler->display->display_options['fields']['counter']['label'] = '#';
  $handler->display->display_options['fields']['counter']['counter_start'] = '1';
  $handler->display->display_options['fields']['counter']['reverse'] = 0;
  /* Field: COVID-19 Data JHU: US County Name */
  $handler->display->display_options['fields']['Admin2']['id'] = 'Admin2';
  $handler->display->display_options['fields']['Admin2']['table'] = 'covid_nineteen_jhu';
  $handler->display->display_options['fields']['Admin2']['field'] = 'Admin2';
  $handler->display->display_options['fields']['Admin2']['label'] = 'County';
  /* Field: SUM(COVID-19 Data JHU: Confirmed cases) */
  $handler->display->display_options['fields']['Confirmed']['id'] = 'Confirmed';
  $handler->display->display_options['fields']['Confirmed']['table'] = 'covid_nineteen_jhu';
  $handler->display->display_options['fields']['Confirmed']['field'] = 'Confirmed';
  $handler->display->display_options['fields']['Confirmed']['group_type'] = 'sum';
  /* Field: SUM(COVID-19 Data JHU: Deaths) */
  $handler->display->display_options['fields']['Deaths']['id'] = 'Deaths';
  $handler->display->display_options['fields']['Deaths']['table'] = 'covid_nineteen_jhu';
  $handler->display->display_options['fields']['Deaths']['field'] = 'Deaths';
  $handler->display->display_options['fields']['Deaths']['group_type'] = 'sum';
  $handler->display->display_options['defaults']['filter_groups'] = FALSE;
  $handler->display->display_options['defaults']['filters'] = FALSE;
  /* Filter criterion: COVID-19 Data JHU: Country */
  $handler->display->display_options['filters']['Country_Region']['id'] = 'Country_Region';
  $handler->display->display_options['filters']['Country_Region']['table'] = 'covid_nineteen_jhu';
  $handler->display->display_options['filters']['Country_Region']['field'] = 'Country_Region';
  $handler->display->display_options['filters']['Country_Region']['value'] = 'US';
  /* Filter criterion: COVID-19 Data JHU: US County Name */
  $handler->display->display_options['filters']['Admin2']['id'] = 'Admin2';
  $handler->display->display_options['filters']['Admin2']['table'] = 'covid_nineteen_jhu';
  $handler->display->display_options['filters']['Admin2']['field'] = 'Admin2';
  $handler->display->display_options['filters']['Admin2']['operator'] = '!=';
  /* Filter criterion: COVID-19 Data JHU: State/Province */
  $handler->display->display_options['filters']['Province_State']['id'] = 'Province_State';
  $handler->display->display_options['filters']['Province_State']['table'] = 'covid_nineteen_jhu';
  $handler->display->display_options['filters']['Province_State']['field'] = 'Province_State';
  $handler->display->display_options['filters']['Province_State']['value'] = 'New York';

  $views = array();
  $views[$view->name] = $view;
  return $views;
}
