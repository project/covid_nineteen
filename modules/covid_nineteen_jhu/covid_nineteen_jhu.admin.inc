<?php

/**
 * @file
 * Administrative functions for COVID-19 Data Source Johns Hopkins University.
 */

/**
 * Form constructor for the COVID-19 configuration form.
 *
 * @see covid_nineteen_jhu_menu()
 */
function covid_nineteen_jhu_admin_configure($form, &$form_state) {
  $form['intro'] = array(
    '#markup' => t('<h2>2019 Novel Coronavirus COVID-19 (2019-nCoV) Data Repository by Johns Hopkins CSSE</h2><b>Please read the Terms of Use on the <a href="@url">GitHub Repository</a> to ensure you are complying with them, before displaying this data on your site.</b>', array('@url' => 'https://github.com/CSSEGISandData/COVID-19')),
  );
  $form['covid_nineteen_jhu'] = array(
    '#tree' => TRUE,
    '#type' => 'fieldset',
  );
  drupal_load('module', 'covid_nineteen_jhu');
  $covid_nineteen_jhu = covid_nineteen_jhu_variable_get();
  $auto_update = $covid_nineteen_jhu['auto_update'];
  $form['covid_nineteen_jhu']['auto_update'] = array(
    '#type' => 'checkbox',
    '#title' => t('Auto update'),
    '#description' => t('Automatically update the data using Drupal cron.'),
    '#default_value' => $auto_update,
  );
  $minutes_between_updates = $covid_nineteen_jhu['minutes_between_updates'];
  $last_update_check_time = $covid_nineteen_jhu['last_update_check_time'];
  $last_checked_string = t('never');
  if ($last_update_check_time) {
    $last_checked_string = format_date($last_update_check_time, 'short');
  }
  $form['covid_nineteen_jhu']['minutes_between_updates'] = array(
    '#type' => 'textfield',
    '#title' => t('Minutes between updates'),
    '#description' => t('How often to check if new data is available to be downloaded (last checked: @last-checked-string).', array('@last-checked-string' => $last_checked_string)),
    '#default_value' => $minutes_between_updates,
    '#required' => TRUE,
  );
  $form['covid_nineteen_jhu']['last_update_check_time'] = array(
    '#type' => 'value',
    '#value' => $last_update_check_time,
  );
  $form['covid_nineteen_jhu']['data_file'] = array(
    '#type' => 'fieldset',
    '#title' => t('Data file URL'),
  );
  $last_commit_date = $covid_nineteen_jhu['data_file']['github']['last_commit_date'];
  $form['covid_nineteen_jhu']['data_file']['github'] = array(
    '#type' => 'fieldset',
    '#title' => t('GitHub Repository'),
    '#description' => t('Downloads the data file from a GitHub repository (downloaded data committed @last-commit-time).', array('@last-commit-time' => format_date(strtotime($last_commit_date), 'short'))),
  );
  $form['covid_nineteen_jhu']['data_file']['github']['last_commit_date'] = array(
    '#type' => 'value',
    '#default_value' => $last_commit_date,
  );
  $repository_owner = $covid_nineteen_jhu['data_file']['github']['owner'];
  $form['covid_nineteen_jhu']['data_file']['github']['owner'] = array(
    '#type' => 'textfield',
    '#title' => t('Repository Owner'),
    '#default_value' => $repository_owner,
    '#description' => t('GitHub user account that owns the repository with the data files.'),
    '#required' => TRUE,
  );
  $repository = $covid_nineteen_jhu['data_file']['github']['repository'];
  $form['covid_nineteen_jhu']['data_file']['github']['repository'] = array(
    '#type' => 'textfield',
    '#title' => t('Git repository'),
    '#default_value' => $repository,
    '#description' => t('Git repository containing the data files.'),
    '#required' => TRUE,
  );
  $branch = $covid_nineteen_jhu['data_file']['github']['branch'];
  $form['covid_nineteen_jhu']['data_file']['github']['branch'] = array(
    '#type' => 'textfield',
    '#title' => t('Git branch'),
    '#default_value' => $branch,
    '#description' => t('Git branch containing the data files.'),
    '#required' => TRUE,
  );
  $data_file_path = $covid_nineteen_jhu['data_file']['github']['data_file_path'];
  $form['covid_nineteen_jhu']['data_file']['github']['data_file_path'] = array(
    '#type' => 'textfield',
    '#title' => t('Data file path'),
    '#default_value' => $data_file_path,
    '#description' => t('Path to the data files.'),
    '#required' => TRUE,
  );
  $data_file_name = $covid_nineteen_jhu['data_file']['github']['data_filename'];
  $form['covid_nineteen_jhu']['data_file']['github']['data_filename'] = array(
    '#type' => 'textfield',
    '#title' => t('Data file name'),
    '#default_value' => $data_file_name,
    '#description' => t('The name of the file containing the data, including its extension.'),
    '#required' => TRUE,
  );
  $example_url = covid_nineteen_jhu_get_github_file_raw_url($covid_nineteen_jhu['data_file']['github']);

  // If a form submit failed validation the "example url" may help with seeing
  // what the problem is. The unsanitized values in $form_state['input'] need to
  // be used for building the url, but $example_url will be sanitized when it is
  // passed through the t() function.
  /* @codingStandardsIgnoreStart */
  $form_state_input = $form_state['input'];
  /* @codingStandardsIgnoreEnd */
  if ($form_state_input) {
    $example_url = covid_nineteen_jhu_get_github_file_raw_url($form_state_input['covid_nineteen_jhu']['data_file']['github']);
  }
  $form['covid_nineteen_jhu']['data_file']['github']['data_file_url_example'] = array(
    '#markup' => t('Download URL: <a href="@data-file-url" target="_blank">@data-file-url</a>', array('@data-file-url' => url($example_url))),
  );
  $override_url = $covid_nineteen_jhu['data_file']['url_override'];
  $form['covid_nineteen_jhu']['data_file']['url_override'] = array(
    '#type' => 'textfield',
    '#title' => t('Alternate URL to fetch data from'),
    '#default_value' => $override_url,
    '#description' => t('An alternate URL to fetch the data from. Leave this blank to build the URL using the above fields.'),
    '#required' => FALSE,
  );
  $form['covid_nineteen_jhu']['update_data_now'] = array(
    '#type' => 'checkbox',
    '#title' => t('Update on submit'),
    '#default_value' => TRUE,
    '#description' => t('Update the data after submitting the form.'),
    '#required' => FALSE,
  );
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Submit'),
  );
  return $form;
}

/**
 * Validation handler for covid_nineteen_jhu_admin_configure().
 */
function covid_nineteen_jhu_admin_configure_validate($form, &$form_state) {
  $minutes_between_updates = &$form_state['values']['covid_nineteen_jhu']['minutes_between_updates'];
  if (!is_numeric($minutes_between_updates) || intval($minutes_between_updates) < 1) {
    form_set_error('minutes_between_updates', t('"Minutes between updates" must be a whole number greater than zero.'));
  }
  else {
    $minutes_between_updates = intval($minutes_between_updates);
  }
  $repository_info = &$form_state['values']['covid_nineteen_jhu']['data_file']['github'];
  $url = covid_nineteen_jhu_get_github_file_raw_url($repository_info);
  if (!valid_url($url)) {
    form_set_error('github', t('Values for the GitHub Repository result in an invalid URL for the data file:  @url', array('@url' => $url)));
  }
}

/**
 * Submit handler for covid_nineteen_jhu_admin_configure().
 */
function covid_nineteen_jhu_admin_configure_submit($form, &$form_state) {
  $covid_nineteen_jhu = $form_state['values']['covid_nineteen_jhu'];
  $github_file_url = 'https://raw.githubusercontent.com/' . $covid_nineteen_jhu['data_file']['github']['owner'] . '/' . $covid_nineteen_jhu['data_file']['github']['repository'] . '/' . $covid_nineteen_jhu['data_file']['github']['branch'] . '/' . $covid_nineteen_jhu['data_file']['github']['data_file_path'] . '/' . $covid_nineteen_jhu['data_file']['github']['data_filename'];
  $covid_nineteen_jhu['data_file']['github']['url'] = $github_file_url;
  covid_nineteen_jhu_variable_set($covid_nineteen_jhu);
  if ($covid_nineteen_jhu['update_data_now']) {
    drupal_load('module', 'covid_nineteen_jhu');
    covid_nineteen_jhu_update_data(TRUE, TRUE);
  }
}
