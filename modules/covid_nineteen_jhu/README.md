COVID-19 Johns Hopkins University CSSE Data
==============

CONTENTS OF THIS FILE
---------------------

* Introduction
* Terms of Use
* Requirements
* Installation
* Configuration
* Maintainers

INTRODUCTION
------------

WARNING: This module is in the early stages of development. It is not
recommended to use on production sites. Carefully read all issues,
documentation, and release notes before installing AND updating the module.

The COVID-19 Johns Hopkins University CSSE Data module allows importing and
displaying of data compiled by Johns Hopkins University about the 2019 Novel
Coronavirus COVID-19 (2019-nCoV). The data is retrieved from a GitHub repository
that Johns Hopkins regularly updates. You can go to the module's configuration
page for more information about the settings for accessing the repository, and
even change details about the repository in case JHU makes changes.

The module includes a View with examples of how to display the data. It can use
Drupal Cron to schedule automatic updates of the data.

Neither this module or its maintainers are affiliated in any way with Johns
Hopkins University, or the sourcing of the data. This module simply provides a
mechanism for downloading and displaying data which has been made available
to the public by Johns Hopkins University. It does not make claims to the
accuracy of the data being downloaded, and does not guarantee the availability
of the data.

This is a sub-module for the COVID-19 module, and does not have a project page
of its own.

For a full description of the COVID-19 module project visit the project page:
<https://www.drupal.org/project/covid_nineteen>

To submit bug reports and feature suggestions, or to track changes:
<https://www.drupal.org/project/issues/covid_nineteen>

Visit the GitHub repository maintained by Johns Hopkins:
<https://github.com/CSSEGISandData/COVID-19>.

TERMS OF USE
------------

Below is a copy of the Terms of Use that is currently shown in the GitHub
repository page which the data is downloaded from. It is your responsibility to
ensure that you are complying with the Terms of Use. I highly recommend that you
regularly visit the GitHub page of the repository to ensure you are complying
with the most recent terms.

Terms of Use:
This GitHub repo and its contents herein, including all data, mapping, and
analysis, copyright 2020 Johns Hopkins University, all rights reserved, is
provided to the public strictly for educational and academic research purposes.
The Website relies upon publicly available data from multiple sources, that do
not always agree. The Johns Hopkins University hereby disclaims any and all
representations and warranties with respect to the Website, including accuracy,
fitness for use, and merchantability. Reliance on the Website for medical
guidance or use of the Website in commerce is strictly prohibited.

REQUIREMENTS
------------

This module requires the following modules:

* [COVID-19](https://www.drupal.org/project/covid_nineteen)
* [Views](https://www.drupal.org/project/views)
* [Block](https://www.drupal.org/project/block)

INSTALLATION
------------

Ensure that all required modules are installed and enabled, then install as you
would normally install a contributed Drupal module:

* Also See <https://drupal.org/documentation/install/modules-themes/modules-7>
for further information.

CONFIGURATION
-------------

* Administration >> Configuration >> Web services >> COVID-19 >> COVID-19 John
Hopkins. You can enable/disable automatically updating the data, and how often
it is updated. You can trigger an update manually by saving the form, and can
change details about the repository in case JHU makes changes to it.
* Go to Administration >> Structure >> Views >> COVID-19 Johns Hopkins
University Example Views for examples of how to display the data.

MAINTAINERS
-----------

Current maintainers:

* Ben Greenberg (runswithscissors) - <http://drupal.org/user/3188825>
