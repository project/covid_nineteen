<?php

/**
 * @file
 * Contains administrative forms and functions for the COVID-19 module.
 */

/**
 * Form constructor for the COVID-19 configuration form.
 *
 * Sub-modules should use hook_form_FORMID_alter() to add fields for enabling
 * and disabling automatic updates in $form['covid_nineteen']['data_sources'].
 *
 * @see covid_nineteen_menu()
 */
function covid_nineteen_admin_display_form($form, &$form_state) {
  $form['covid_nineteen']['data_sources'] = array(
    '#type' => 'fieldset',
    '#title' => t('Data Sources'),
  );
  return system_settings_form($form);
}
