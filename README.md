COVID-19
==============

CONTENTS OF THIS FILE
---------------------

* Introduction
* Requirements
* Installation
* Configuration
* Maintainers

INTRODUCTION
------------

The COVID-19 module allows importing and displaying of data related to the 2019
Novel Coronavirus COVID-19 (2019-nCoV) from multiple sources. The intention is
not only to make it easier to display and update data, but to select which data
set to use depending on the information being requested. For example, one data
set would be used for country-level data, but there may be a "better" data
source to use for a particular state, province, or county. To be useful, a
sub-module for a data source needs to be enabled. Currently there are two
sub-modules included with this module:

* Johns Hopkins University. Contains data for many countries, states/provinces,
as well as county-level data for the United States. It is your responsibility to
ensure that you are complying with the Terms of Use for the data. You should
read the Terms of Use on the repository page before enabling the module, and
regularly thereafter to ensure you continue to be in compliance with its terms:
<https://github.com/CSSEGISandData/COVID-19>.
* The Covid Tracking Project. Contains data for the United States, and only down
to the state-level. However, its data is licensed under the Apache License,
Version 2:
<https://github.com/COVID19Tracking/covid-tracking-data/blob/master/LICENSE>

For a full description of the project visit the project page:
<https://www.drupal.org/project/covid_nineteen>

To submit bug reports and feature suggestions, or to track changes:
<https://www.drupal.org/project/issues/covid_nineteen>

REQUIREMENTS
------------

This module does not have any requirements, although to be useful one of its
sub-modules needs to be enabled.

INSTALLATION
------------

Install as you would normally install a contributed Drupal module (and at least
one sub-module):

* Also See <https://drupal.org/documentation/install/modules-themes/modules-7>
for further information.

CONFIGURATION
-------------

* Administration >> Configuration >> Web services >> COVID-19

MAINTAINERS
-----------

Current maintainers:

* Ben Greenberg (runswithscissors) - <http://drupal.org/user/3188825>
